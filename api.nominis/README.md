# Nominis - API

> v1.0.0

API pour la plateforme Nominis

## Installation

> Environnement : Ubuntu 18.04

- Installer node js

- Installer mongoDB

- Installer les dépencances pour bcrypt

```sh
$ sudo apt-get install -y build-essential python
```

- Mise en place de la configuration

```sh
$ cp .env.example .env
```

et changer la valeur des variables d'environnement suivant votre environnement

- Installer les dépendances du projet

```sh
$ npm install
```

- Lancer le serveur

```sh
$ npm run serve
```
