const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
require('dotenv').config()

const config = require('./config')

const app = express()

if (config.APP_ENV === 'dev') app.use(morgan('combined'))

// Middleware
app.use(bodyParser.json())

// Routes
app.use('/', (req, res) => {
  res.json({ info: 'Nominis - Les recours fonciers + API' })
})

// Start the server
app.listen(config.APP_PORT)
console.log(`Server listening at ${config.APP_HOST}:${config.APP_PORT}`)
