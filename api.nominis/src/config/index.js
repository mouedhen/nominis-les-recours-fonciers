module.exports = {
  APP_NAME: process.env.APP_NAME || 'Nominis',
  APP_ENV: process.env.APP_ENV || 'dev',
  APP_HOST: process.env.APP_HOST || 'http://localhost',
  APP_PORT: process.env.APP_PORT || 3000
}
