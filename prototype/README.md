# Nominis - Les recours fonciers+

> v0.0.1-alpha

La version 0.0.1 est une version prototype. 
Elle expose les principales fonctionnalités qui devront êtres présentes dans la version finale.

## Installation - dev

> dépendances : NodeJs

```bash
$ npm install
$ npm run serve
```

## Déploiement

- Générer la version de distribution

```bash
$ npm install
$ npm run build
```

- Configurer l'hôte (apache2 / nginx) afin de servir les fichiers qui se trouvent sous le dossier ./dist

- Afin d'assurer la sécurité de la plateforme, il est recommandé de rajouter une clé SSL au domaine associé à l'application.
