import AbstractModel from '../core/abstract-model'
import firestore from '../../../firestore'

class Office extends AbstractModel {
  constructor ({id, name}) {
    super()
    this.serialize({id, name})
    this.config = {
      collection: firestore.collection('config').doc('configNominisResidentielle').collection('offices'),
      archiveCollection: firestore.collection('archives').doc('nominisArchives').collection('config').doc('configNominisResidentielle').collection('offices')
    }
  }

  serialize ({id, name}) {
    this.id = id
    this.name = name
  }
}

export default Office
