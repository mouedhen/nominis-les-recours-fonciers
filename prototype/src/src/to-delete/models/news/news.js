import moment from 'moment-timezone'
import firebase from '@/firebase'
import { newsCollection } from '../core/helpers'
import ActDocument from '../acts/act-document'

moment.locale('fr')

class News {
  constructor ({actID, type, title, body, file, isPublished, createdAt, updatedAt, publishedAt, id}) {
    this.serialize({actID, type, title, body, file, isPublished, createdAt, updatedAt, publishedAt, id})
  }

  serialize ({actID, type, title, body, file, isPublished, createdAt, updatedAt, publishedAt, id}) {
    this.actID = actID
    this.type = type
    this.title = title
    this.body = body
    this.file = file
    this.isPublished = isPublished
    this.createdAt = createdAt
    this.updatedAt = updatedAt
    this.publishedAt = publishedAt
    this.id = id
  }

  async getImg () {
    return new ActDocument({id: this.actID}).getByID().then(actDocument => {
      return actDocument.file
    }).catch(error => {
      console.log(error)
    })
  }

  async update () {
    var data = {
      title: this.title,
      body: this.body,
      type: this.type,
      isPublished: this.isPublished,
      updatedAt: firebase.firestore.FieldValue.serverTimestamp()
    }
    return this.getIsPublished().then(isPublished => {
      if (!isPublished) {
        if (this.isPublished) {
          data.publishedAt = firebase.firestore.FieldValue.serverTimestamp()
          return newsCollection.doc(this.id).update(data)
        } else {
          return newsCollection.doc(this.id).update(data)
        }
      } else {
        return newsCollection.doc(this.id).update(data)
      }
    })
  }

  async remove () {
    return newsCollection.doc(this.id).delete()
  }

  async getLastTenNewsArray () {
    let query = newsCollection.where('isPublished', '==', true)
    return query.orderBy('publishedAt', 'desc').limit(10).get().then(snapshot => {
      var news = []
      snapshot.forEach(doc => {
        var singleNews = {
          id: doc.id,
          actID: doc.data().actID,
          title: doc.data().title,
          body: doc.data().body,
          file: doc.data().file,
          type: doc.data().type,
          isPublished: doc.data().isPublished,
          createdAt: this.convertTimestamp2String(doc.data().createdAt),
          updatedAt: this.convertTimestamp2String(doc.data().updatedAt),
          publishedAt: this.convertTimestamp2String(doc.data().publishedAt),
          isFavorite: false
        }
        if (!doc.data().publishedAt) singleNews.publishedAt = ''
        if (!doc.data().updatedAt) singleNews.updatedAt = ''
        news.push(singleNews)
      })
      return news
    })
  }

  async setPublished (isPublished) {
    if (isPublished) {
      return newsCollection.doc(this.id).update({
        isPublished: isPublished,
        updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
        publishedAt: firebase.firestore.FieldValue.serverTimestamp()
      })
    } else {
      return newsCollection.doc(this.id).update({
        isPublished: isPublished,
        updatedAt: firebase.firestore.FieldValue.serverTimestamp()
      })
    }
  }

  async getIsPublished () {
    return newsCollection.doc(this.id).get().then(news => {
      return news.isPublished
    })
  }

  convertTimestamp2String (timestamp) {
    let tz = moment.tz.guess()
    if (moment(timestamp).isDST()) {
      timestamp -= 3600000
    }
    return moment(timestamp).tz(tz).format('LLL')
  }
}

export default News
