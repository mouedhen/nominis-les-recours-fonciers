import firebase from '@/firebase'
import 'firebase/database'
import { rolesCollection } from '../core/helpers'

class Role {
  constructor ({id, designation, label}) {
    this.serialize({id, designation, label})
    this.ref = firebase.database().ref('roles')
  }

  serialize ({id, designation, label}) {
    this.id = id
    this.designation = designation
    this.label = label
  }

  async get () {
    return rolesCollection.get().then(snapshot => {
      var roles = []
      snapshot.forEach(doc => {
        let data = doc.data()
        roles.push(new Role({id: doc.id, designation: data.designation, label: data.label}))
      })
      return roles
    })
  }
}

export default Role
