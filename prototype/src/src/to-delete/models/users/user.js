import firebase from '@/firebase'
import 'firebase/database'
import { usersCollection, convertTimestamp2String } from '../core/helpers'

class User {
  constructor ({firstName, lastName, phoneNumber, email, password, passwordConfirm, role, isActive, uid, createdAt, updatedAt}) {
    this.serialize({
      firstName,
      lastName,
      phoneNumber,
      email,
      password,
      passwordConfirm,
      role,
      isActive,
      uid,
      createdAt,
      updatedAt
    })
  }

  serialize ({firstName, lastName, phoneNumber, email, password, passwordConfirm, role, isActive, uid, createdAt, updatedAt}) {
    this.firstName = firstName
    this.lastName = lastName
    this.phoneNumber = phoneNumber
    this.email = email
    this.password = password
    this.passwordConfirm = passwordConfirm
    this.role = role
    this.isActive = isActive
    this.uid = uid
    this.createdAt = createdAt
    this.updatedAt = updatedAt
  }

  async createUser (email, password, role, isActive) {
    let createUser = firebase.functions().httpsCallable('createUser')
    return createUser({email: email, password: password, isActive: !isActive, role: role}).then(function (result) {
      return (result.data.uid)
    })
  }

  async updateUser (email, role, isActive, uid) {
    let updateUser = firebase.functions().httpsCallable('updateUser')
    return updateUser({email: email, isActive: !isActive, role: role, uid: uid}).then(() => {
      return this.update()
    })
  }

  async updateIsActive () {
    let updateUserActivity = firebase.functions().httpsCallable('updateUserActivity')
    console.log('isactive', this.isActive)
    return updateUserActivity({isActive: this.isActive, uid: this.uid}).then(() => {
      return this.updateUserActivity(this.isActive)
    })
  }

  async updateUserActivity () {
    return usersCollection.doc(this.uid).update({
      isActive: !this.isActive,
      updatedAt: firebase.firestore.FieldValue.serverTimestamp()
    }).then(() => {
      return this.updatedAt
    })
  }
  async deleteUser (uid) {
    let deleteUser = firebase.functions().httpsCallable('deleteUser')
    return deleteUser({uid: uid})
  }

  async create () {
    if (this.isActive == null) this.isActive = false
    let data = {
      email: this.email,
      role: this.role,
      isActive: this.isActive,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      updatedAt: firebase.firestore.FieldValue.serverTimestamp()
    }
    if (this.firstName) data.firstName = this.firstName
    if (this.lastName) data.lastName = this.lastName
    if (this.phoneNumber) data.phoneNumber = this.phoneNumber
    return this.createUser(this.email, this.password, this.role, this.isActive).then(uid => {
      this.uid = uid
      let newUserRef = usersCollection.doc(uid)
      return newUserRef.set(data)
    })
  }

  async update () {
    if (this.isActive == null) this.isActive = false
    let data = {
      isActive: this.isActive,
      updatedAt: firebase.firestore.FieldValue.serverTimestamp()
    }
    if (this.firstName) data.firstName = this.firstName
    if (this.lastName) data.firstName = this.lastName
    if (this.email) data.email = this.email
    if (this.role) data.role = this.role
    if (this.phoneNumber) data.firstName = this.phoneNumber
    return this.updateUser(this.email, this.role, this.isActive, this.uid).then(uid => {
      return usersCollection.doc(this.uid).update(data)
    })
  }

  async remove () {
    return this.deleteUser(this.uid).then(() => {
      return usersCollection.doc(this.uid).delete()
    })
  }

  async signInWithEmail (email) {
    return firebase.auth().signInWithEmailAndPassword(email, this.password)
  }

  async signOut () {
    return firebase.auth().signOut()
  }

  async resetPassword (email) {
    return firebase.auth().sendPasswordResetEmail(email)
  }

  async getUserById () {
    return usersCollection.doc(this.uid).get().then(document => {
      this.isActive = document.data().isActive
      this.role = document.data().role
      this.phoneNumber = document.data().phoneNumber
      this.email = document.data().email
      this.lastName = document.data().lastName
      this.firstName = document.data().firstName
      this.createdAt = document.data().createdAt
      this.updatedAt = document.data().updatedAt
      return this
    })
  }

  async getUsers () { // returns an array of users objects (not instances)
    return usersCollection.get().then(snapshot => {
      let users = []
      snapshot.forEach(doc => {
        let user = {
          uid: doc.id,
          createdAt: convertTimestamp2String(doc.data().createdAt),
          updatedAt: convertTimestamp2String(doc.data().updatedAt),
          firstName: doc.data().firstName,
          lastName: doc.data().lastName,
          phoneNumber: doc.data().phoneNumber,
          email: doc.data().email,
          role: doc.data().role,
          isActive: doc.data().isActive
        }
        users.push(user)
      })
      return users
    })
  }

  async setActive (isActive) {
    this.isActive = isActive
    return this.updateIsActive().then(() => {
      return this.updateUserActivity()
    })
  }

  getProfile () {
    return firebase.auth().currentUser
  }
}

export default User
