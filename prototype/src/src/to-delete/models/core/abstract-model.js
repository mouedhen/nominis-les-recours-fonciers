import { cleanObject } from './helpers'

class AbstractModel {
  constructor () {
    this.config = {collection: undefined}
    if (new.target === AbstractModel) {
      throw new TypeError('Cannot construct Abstract instances directly')
    }
  }

  serialize () {
    throw new Error('This method should be implemented')
  }

  /**
   * clean object for database manipulation
   * @returns {{}}
   */
  clean () {
    let record = this
    delete record.inscriptionDateDisplay
    delete record.deadlineDisplay
    delete record.ownerDisplay
    delete record.sellerDisplay
    delete record.juristDisplay
    delete record.addressesDisplay
    return cleanObject(record)
  }

  /**
   * Fetch all records from the database
   * @returns {Promise<Array>}
   */
  async fetchAll () {
    // @TODO add factory to return instance
    return this.config.collection.get()
      .then(snapshot => {
        let records = []
        snapshot.forEach(doc => {
          records.push({id: doc.id, ...doc.data()})
        })
        return records
      })
  }

  async fetch (id) {
    return this.config.collection.doc(id).get()
      .then(doc => {
        this.serialize({id: doc.id, ...doc.data()})
        return this
      })
  }

  /**
   * Fetch records from the database and handle filters
   * @param filters
   * @returns {Promise<Array>}
   */
  async fetchQuery (filters) {
    let query = this.config.collection
    filters.forEach(x => {
      query = query.where(x.params, x.operation, x.value)
    })
    return query.get()
      .then(snapshot => {
        let records = []
        snapshot.forEach(doc => {
          records.push({id: doc.id, ...doc.data()})
        })
        return records
      })
  }

  async save () {
    if (this.id === undefined || this.id === null) {
      return this.create()
    }
    return this.update()
  }

  async create () {
    return this.config.collection.add({...this.clean()})
      .then(docRef => {
        this.id = docRef.id
        return this
      })
  }

  async update () {
    return this.config.collection.doc(this.id).update({...this.clean()}).then(() => this)
  }

  /**
   * Delete the record from the database
   * @returns {Promise<void>}
   */
  async delete () {
    return this.config.collection.doc(this.id).delete()
  }

  async archive () {
    let archiveDoc = this
    return this.config.archiveCollection.add({...this.clean()})
      .then(() => {
        return archiveDoc.delete()
      })
  }
}

export default AbstractModel
