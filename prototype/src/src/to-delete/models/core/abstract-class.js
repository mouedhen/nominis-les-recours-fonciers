import { cleanObject } from './helpers'

class AbstractClass {
  clean () {
    return cleanObject(this)
  }
}

export default AbstractClass
