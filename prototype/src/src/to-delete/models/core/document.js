import firebase from '../../firebase'
import 'firebase/storage'

class Document {
  constructor ({file}) {
    this.file = file
    this.name = (+new Date()) + file.name
    this.metadata = {contentType: file.type}
    this.ref = firebase.storage().ref()
    this.task = this.ref.child(this.name).put(this.file, this.metadata)
  }

  async uploadFile () {
    return this.task.then((snapshot) => {
      return snapshot.ref.getDownloadURL()
        .then(url => {
          return {
            name: snapshot.metadata.fullPath,
            type: snapshot.metadata.contentType,
            url
          }
        })
    })
  }
}

export default Document
