import firestore from '../../../firestore'
import moment from 'moment-timezone'
moment.locale('fr')
/**
 * @deprecated
 * @type {{NEW: string, AVAILABLE: string, BEING_PROCESSED: string, PROCESSED: string, ARCHIVED: string}}
 */
export const docStatus = {
  NEW: 'nouveau',
  AVAILABLE: 'disponible',
  BEING_PROCESSED: 'en cours de traitement',
  PROCESSED: 'traité',
  ARCHIVED: 'archivé'
}
export const roles = {
  CUSTOMER: {
    label: 'customer',
    designation: 'Client'
  },
  VERIFICATION_OFFICIER: {
    label: 'verification_officier',
    designation: 'Agent de vérification'
  },
  ENTRY_OFFICIER: {
    label: 'entry_officier',
    designation: 'Agent de saisie'
  },
  ADMINISTRATOR: {
    label: 'administrator',
    designation: 'Administrateur'
  },
  COLLECTOR: {
    label: 'collector',
    designation: 'Agent de collecte'
  }
}

export const usersCollection = firestore.collection('users')
export const rolesCollection = firestore.collection('roles')
export const actsCollection = firestore.collection('act-documents')
export const newsCollection = firestore.collection('news')
export const favoritesCollection = firestore.collection('favorites')

/**
 * @deprecated
 * @type {{BEING_PROCESSED: string, PROCESSED: string, VERIFIED: string, PUBLISHED: string, ARCHIVED: string}}
 */
export const actStatus = {
  BEING_PROCESSED: 'en cours de traitement',
  PROCESSED: 'traité',
  VERIFIED: 'vérifié',
  PUBLISHED: 'publié',
  ARCHIVED: 'archivé'
}

/**
 * Clean an object form all undefined properties
 * @param obj
 * @returns {{}}
 */
export function cleanObject (obj) {
  return Object.keys(obj).filter(key => obj[key]).reduce(
    (newObj, key) => {
      if (key !== 'config') {
        newObj[key] = obj[key]
      }
      return newObj
    }, {}
  )
}

export function convertTimestamp2String (date) {
  if (date) {
    let timestamp = date.seconds
    // let tz = moment.tz.guess()
    // if (moment.unix(timestamp).isDST()) {
    //   timestamp -= 3600
    // }
    return moment.unix(timestamp).utc().format('l LT')
  }
}
/*
function writeCities () {
  let s = 'Acton Vale\tActon\tMontérégie\n' +
    '      Alma\tLac-Saint-Jean-Est\tSaguenay–Lac-Saint-Jean\n' +
    '      Amos\tAbitibi\tAbitibi-Témiscamingue\n' +
    '      Amqui\tLa Matapédia\tBas-Saint-Laurent\n' +
    '      Asbestos\tLes Sources\tEstrie\n' +
    '      Baie-Comeau\tManicouagan\tCôte-Nord\n' +
    '      Baie-d\'Urfé\tÎle de Montréal\tMontréal\n' +
    '      Baie-Saint-Paul\tCharlevoix\tCapitale-Nationale\n' +
    '      Barkmere\tLes Laurentides\tLaurentides\n' +
    '      Beaconsfield\tÎle de Montréal\tMontréal\n' +
    '      Beauceville\tRobert-Cliche\tChaudière-Appalaches\n' +
    '      Beauharnois\tBeauharnois-Salaberry\tMontérégie\n' +
    '      Beaupré\tLa Côte-de-Beaupré\tCapitale-Nationale\n' +
    '      Bécancour\tBécancour\tCentre-du-Québec\n' +
    '      Bedford\tBrome-Missisquoi\tEstrie\n' +
    '      Belleterre\tTémiscamingue\tAbitibi-Témiscamingue\n' +
    '      Belœil\tLa Vallée-du-Richelieu\tMontérégie\n' +
    '      Berthierville\tD\'Autray\tLanaudière\n' +
    '      Blainville\tThérèse-De Blainville\tLaurentides\n' +
    '      Bois-des-Filion\tThérèse-De Blainville\tLanaudière\n' +
    '      Boisbriand\tThérèse-De Blainville\tLanaudière\n' +
    '      Bonaventure\tBonaventure\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Boucherville\tAgglomération de Longueuil\tMontérégie\n' +
    '      Bromont\tLa Haute-Yamaska\tEstrie\n' +
    '      Brossard\tAgglomération de Longueuil\tMontérégie\n' +
    '      Brownsburg-Chatham\tArgenteuil\tLaurentides\n' +
    '      Cabano\tTémiscouata\tBas-Saint-Laurent\n' +
    '      Candiac\tRoussillon\tMontérégie\n' +
    '      Cap-Chat\tLa Haute-Gaspésie\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Cap-Santé\tPortneuf\tCapitale-Nationale\n' +
    '      Carignan\tLa Vallée-du-Richelieu\tMontérégie\n' +
    '      Carleton-sur-Mer\tAvignon\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Causapscal\tLa Matapédia\tBas-Saint-Laurent\n' +
    '      Chambly\tLa Vallée-du-Richelieu\tMontérégie\n' +
    '      Chandler\tLe Rocher-Percé\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Chapais\tNord-du-Québec\tNord-du-Québec\n' +
    '      Charlemagne\tL\'Assomption\tLanaudière\n' +
    '      Château-Richer\tLa Côte-de-Beaupré\tCapitale-Nationale\n' +
    '      Châteauguay\tRoussillon\tMontérégie\n' +
    '      Chibougamau\tNord-du-Québec\tNord-du-Québec\n' +
    '      Clermont\tCharlevoix-Est\tCapitale-Nationale\n' +
    '      Coaticook\tCoaticook\tEstrie\n' +
    '      Contrecœur\tLajemmerais\tCentre-du-Québec\n' +
    '      Cookshire-Eaton\tLe Haut-Saint-François\tEstrie\n' +
    '      Côte-Saint-Luc\tÎle de Montréal\tMontréal\n' +
    '      Coteau-du-Lac\tVaudreuil-Soulanges (municipalité régionale de comté)\tMontérégie\n' +
    '      Cowansville\tBrome-Missisquoi\tMontérégie\n' +
    '      Danville\tLes Sources\tCentre-du-Québec\n' +
    '      Daveluyville\tArthabaska\tCentre-du-Québec\n' +
    '      Dégelis\tTémiscouata\tBas-Saint-Laurent\n' +
    '      Delson\tRoussillon\tEstrie\n' +
    '      Desbiens\tLac-Saint-Jean-Est\tSaguenay–Lac-Saint-Jean\n' +
    '      Deux-Montagnes\tDeux-Montagnes\tLaurentides\n' +
    '      Disraeli\tLes Appalaches\tChaudière-Appalaches\n' +
    '      Dolbeau-Mistassini\tMaria-Chapdelaine\tSaguenay–Lac-Saint-Jean\n' +
    '      Dollard-des-Ormeaux\tÎle de Montréal\tMontréal\n' +
    '      Donnacona\tPortneuf\tCapitale-Nationale\n' +
    '      Dorval\tÎle de Montréal\tMontréal\n' +
    '      Drummondville\tDrummond\tCentre-du-Québec\n' +
    '      Dunham\tBrome-Missisquoi\tMontérégie\n' +
    '      Duparquet\tAbitibi-Ouest\tAbitibi-Témiscamingue\n' +
    '      East Angus\tLe Haut-Saint-François\tEstrie\n' +
    '      L\'Épiphanie\tL\'Assomption\tLanaudière\n' +
    '      Estérel\tLes Pays-d\'en-Haut\tLaurentides\n' +
    '      Farnham\tBrome-Missisquoi\tMontérégie\n' +
    '      Fermont\tCaniapiscau\tCôte-Nord\n' +
    '      Forestville\tLa Haute-Côte-Nord\tCôte-Nord\n' +
    '      Fossambault-sur-le-Lac\tLa Jacques-Cartier\tCapitale-Nationale\n' +
    '      Gaspé\tLa Côte-de-Gaspé\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Gatineau\tGatineau\tOutaouais\n' +
    '      Gracefield\tLa Vallée-de-la-Gatineau\tOutaouais\n' +
    '      Granby\tLa Haute-Yamaska\tMontérégie\n' +
    '      Grande-Rivière\tLe Rocher-Percé\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Hampstead\tÎle de Montréal\tMontréal\n' +
    '      Hudson\tVaudreuil-Soulanges\tMontérégie\n' +
    '      Huntingdon\tLe Haut-Saint-Laurent\tMontérégie\n' +
    '      L\'Île-Cadieux\tVaudreuil-Soulanges\tMontérégie\n' +
    '      L\'Île-Dorval\tÎle de Montréal\tMontréal\n' +
    '      L\'Île-Perrot\tVaudreuil-Soulanges\tMontréal\n' +
    '      Joliette\tJoliette\tLanaudière\n' +
    '      Kingsey Falls\tArthabaska\tEstrie\n' +
    '      Kirkland\tÎle de Montréal\tAgglomération de Montréal\n' +
    '      L\'Ancienne-Lorette\tQuébec\tCapitale-Nationale\n' +
    '      L\'Assomption\tL\'Assomption\tLanaudière\n' +
    '      L\'Île-Perrot\tVaudreuil-Soulanges\tMontérégie\n' +
    '      La Malbaie\tCharlevoix-Est\tCapitale-Nationale\n' +
    '      La Pocatière\tKamouraska\tBas-Saint-Laurent\n' +
    '      La Prairie\tRousillion\tMontérégie\n' +
    '      La Sarre\tAbitibi-Ouest\tAbitibi-Témiscamingue\n' +
    '      La Tuque\tMauricie\tMauricie\n' +
    '      Lac-Brome\tBrome-Missisquoi\tMontérégie\n' +
    '      Lac-Delage\tLa Jacques-Cartier\tCapitale-Nationale\n' +
    '      Lac-Mégantic\tLe Granit\tEstrie\n' +
    '      Lac-Saint-Joseph\tLa Jacques-Cartier\tCapitale-Nationale\n' +
    '      Lac-Sergent\tPortneuf\tCapitale-Nationale\n' +
    '      Lachute\tArgenteuil\tLaurentides\n' +
    '      Laval\tLaval\tLaval\n' +
    '      Lavaltrie\tD\'Autray\tLanaudière\n' +
    '      Lebel-sur-Quévillon\tNord-du-Québec\tNord-du-Québec\n' +
    '      Léry\tRoussillon\tMontérégie\n' +
    '      Lévis\tLévis\tChaudière-Appalaches\n' +
    '      Longueuil\tLongueuil\tMontérégie\n' +
    '      Lorraine\tThérèse-De Blainville\tLaurentides\n' +
    '      Louiseville\tMaskinongé\tMauricie\n' +
    '      Macamic\tAbitibi-Ouest\tAbitibi-Témiscamingue\n' +
    '      Magog\tMemphrémagog\tEstrie\n' +
    '      Malartic\tLa Vallée-de-l\'Or\tAbitibi-Témiscamingue\n' +
    '      Maniwaki\tLa Vallée-de-la-Gatineau\tOutaouais\n' +
    '      Marieville\tRouville\tMontérégie\n' +
    '      Mascouche\tLes Moulins\tLanaudière\n' +
    '      Matagami\tNord-du-Québec\tNord-du-Québec\n' +
    '      Matane\tMatane\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Mercier\tRoussillon\tMontérégie\n' +
    '      Métabetchouan–Lac-à-la-Croix\tLac-Saint-Jean-Est\tSaguenay–Lac-Saint-Jean\n' +
    '      Métis-sur-Mer\tLa Mitis\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Mirabel (Québec)\tMirabel\tLaurentides\n' +
    '      Mont-Joli\tLa Mitis\tBas-Saint-Laurent\n' +
    '      Mont-Laurier\tAntoine-Labelle\tLaurentides\n' +
    '      Mont-Royal\tÎle de Montréal\tMontréal\n' +
    '      Mont-Saint-Hilaire\tLa Vallée-du-Richelieu\tMontérégie\n' +
    '      Mont-Tremblant\tLes Laurentides\tLaurentides\n' +
    '      Montmagny\tMontmagny\tChaudière-Appalaches\n' +
    '      Montréal\tÎle de Montréal\tMontréal\n' +
    '      Montréal-Est\tÎle de Montréal\tMontréal\n' +
    '      Montréal-Ouest\tÎle de Montréal\tMontréal\n' +
    '      Murdochville\tLa Côte-de-Gaspé\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Neuville\tPortneuf\tCapitale-Nationale\n' +
    '      New Richmond\tBonaventure\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Nicolet\tNicolet-Yamaska\tCentre-du-Québec\n' +
    '      Normandin\tMaria-Chapdelaine\tSaguenay–Lac-Saint-Jean\n' +
    '      Notre-Dame-de-l\'Île-Perrot\tVaudreuil-Soulanges\tMontérégie\n' +
    '      Notre-Dame-des-Prairies\tJoliette\tLanaudière\n' +
    '      Notre-Dame-du-Lac\tTémiscouata\tBas-Saint-Laurent\n' +
    '      Otterburn Park\tLa Vallée-du-Richelieu\tMontérégie\n' +
    '      Paspébiac\tBonaventure\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Percé\tLe Rocher-Percé\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Pincourt\tVaudreuil-Soulanges\tMontérégie\n' +
    '      Plessisville\tL\'Érable\tCentre-du-Québec\n' +
    '      La Pocatière\tKamouraska\tBas-Saint-Laurent\n' +
    '      Pohénégamook\tTémiscouata\tBas-Saint-Laurent\n' +
    '      Pointe-Claire\tÎle de Montréal\tMontréal\n' +
    '      Pont-Rouge\tPortneuf\tCapitale-Nationale\n' +
    '      Port-Cartier\tSept-Rivières\tCôte-Nord\n' +
    '      Portneuf\tPortneuf\tCapitale-Nationale\n' +
    '      La Prairie\tRoussillon\tMontérégie\n' +
    '      Prévost\tLa Rivière-du-Nord\tLaurentides\n' +
    '      Princeville\tL\'Érable\tCentre-du-Québec\n' +
    '      Québec\tQuébec\tCapitale-Nationale\n' +
    '      Repentigny\tL\'Assomption\tLanaudière\n' +
    '      Richelieu\tRouville\tMontérégie\n' +
    '      Richmond\tLe Val-Saint-François\tEstrie\n' +
    '      Rimouski\tRimouski-Neigette\tBas-Saint-Laurent\n' +
    '      Rivière-du-Loup\tRivière-du-Loup\tBas-Saint-Laurent\n' +
    '      Rivière-Rouge\tAntoine-Labelle\tLaurentides\n' +
    '      Roberval\tLe Domaine-du-Roy\tSaguenay–Lac-Saint-Jean\n' +
    '      Rosemère\tThérèse-De Blainville\tLaurentides\n' +
    '      Rouyn-Noranda\tRouyn-Noranda\tAbitibi-Témiscamingue\n' +
    '      Saguenay\tLe Saguenay-et-son-Fjord\tSaguenay–Lac-Saint-Jean\n' +
    '      Saint-Augustin-de-Desmaures\tQuébec\tCapitale-Nationale\n' +
    '      Saint-Basile\tPortneuf\tCapitale-Nationale\n' +
    '      Saint-Basile-le-Grand\tLa Vallée-du-Richelieu\tMontérégie\n' +
    '      Saint-Bruno-de-Montarville\tLongueuil\tMontérégie\n' +
    '      Saint-Césaire\tRouville\tMontérégie\n' +
    '      Saint-Constant\tRoussillon\tMontérégie\n' +
    '      Saint-Eustache\tDeux-Montagnes\tLaurentides\n' +
    '      Saint-Félicien\tLe Domaine-du-Roy\tSaguenay–Lac-Saint-Jean\n' +
    '      Saint-Gabriel\tD\'Autray\tLanaudière\n' +
    '      Saint-Georges\tBeauce-Sartigan\tChaudière-Appalaches\n' +
    '      Saint-Hyacinthe\tLes Maskoutains\tMontérégie\n' +
    '      Saint-Jean-sur-Richelieu\tLe Haut-Richelieu\tMontérégie\n' +
    '      Saint-Jérôme\tLa Rivière-du-Nord\tLaurentides\n' +
    '      Saint-Joseph-de-Beauce\tRobert-Cliche\tChaudière-Appalaches\n' +
    '      Saint-Joseph-de-Sorel\tPierre-De Saurel\tMontérégie\n' +
    '      Saint-Lambert\tLongueuil\tMontérégie\n' +
    '      Saint-Lazare\tVaudreuil-Soulanges\tMontérégie\n' +
    '      Saint-Lin-Laurentides\tMontcalm\tLaurentides\n' +
    '      Saint-Marc-des-Carrières\tPortneuf\tCapitale-Nationale\n' +
    '      Saint-Ours\tPierre-De Saurel\tMontérégie\n' +
    '      Saint-Pamphile\tL\'Islet\tBas-Saint-Laurent\n' +
    '      Saint-Pascal\tKamouraska\tBas-Saint-Laurent\n' +
    '      Saint-Pie\tLes Maskoutains\tMontérégie\n' +
    '      Saint-Raymond\tPortneuf\tCapitale-Nationale\n' +
    '      Saint-Rémi\tLes Jardins-de-Napierville\tMontérégie\n' +
    '      Saint-Sauveur\tLes Pays-d\'en-Haut\tLaurentides\n' +
    '      Saint-Tite\tMékinac\tMauricie\n' +
    '      Sainte-Adèle\tLes Pays-d\'en-Haut\tLaurentides\n' +
    '      Sainte-Agathe-des-Monts\tLes Laurentides\tLaurentides\n' +
    '      Sainte-Anne-de-Beaupré\tLa Côte-de-Beaupré\tCapitale-Nationale\n' +
    '      Sainte-Anne-de-Bellevue\tÎle de Montréal\tMontréal\n' +
    '      Sainte-Anne-des-Monts\tLa Haute-Gaspésie\tGaspésie–Îles-de-la-Madeleine\n' +
    '      Sainte-Anne-des-Plaines\tThérèse-De Blainville\tLaurentides\n' +
    '      Sainte-Catherine\tRoussillon\tMontérégie\n' +
    '      Sainte-Catherine-de-la-Jacques-Cartier\tLa Jacques-Cartier\tCapitale-Nationale\n' +
    '      Sainte-Julie\tLajemmerais\tMontérégie\n' +
    '      Sainte-Marguerite-du-Lac-Masson\tLes Pays-d\'en-Haut\tLaurentides\n' +
    '      Sainte-Marie\tLa Nouvelle-Beauce\tChaudière-Appalaches\n' +
    '      Sainte-Marthe-sur-le-Lac\tDeux-Montagnes\tLaurentides\n' +
    '      Sainte-Thérèse\tThérèse-De Blainville\tLaurentides\n' +
    '      Salaberry-de-Valleyfield\tBeauharnois-Salaberry\tMontérégie\n' +
    '      La Sarre\tAbitibi-Ouest\tAbitibi-Témiscamingue\n' +
    '      Schefferville\tCaniapiscau\tNord-du-Québec\n' +
    '      Scotstown\tLe Haut-Saint-François\tEstrie\n' +
    '      Senneterre\tLa Vallée-de-l\'Or\tAbitibi-Témiscamingue\n' +
    '      Sept-Îles\tSept-Rivières\tCôte-Nord\n' +
    '      Shawinigan\tShawinigan\tMauricie\n' +
    '      Sherbrooke\tSherbrooke\tEstrie\n' +
    '      Sorel-Tracy\tPierre-De Saurel\tMontérégie\n' +
    '      Stanstead\tMemphrémagog\tEstrie\n' +
    '      Sutton\tBrome-Missisquoi\tMontérégie\n' +
    '      Témiscaming\tTémiscamingue\tAbitibi-Témiscamingue\n' +
    '      Terrebonne\tLes Moulins\tLanaudière\n' +
    '      Thetford Mines\tLes Appalaches\tChaudière-Appalaches\n' +
    '      Thurso\tPapineau\tOutaouais\n' +
    '      Trois-Pistoles\tLes Basques\tBas-Saint-Laurent\n' +
    '      Trois-Rivières\tFrancheville\tMauricie\n' +
    '      La Tuque\tLa Tuque\tMauricie\n' +
    '      Val-d\'Or\tLa Vallée-de-l\'Or\tAbitibi-Témiscamingue\n' +
    '      Valcourt\tLe Val-Saint-François\tEstrie\n' +
    '      Varennes\tLajemmerais\tMontérégie\n' +
    '      Vaudreuil-Dorion\tVaudreuil-Soulanges\tMontérégie\n' +
    '      Victoriaville\tArthabaska\tCentre-du-Québec\n' +
    '      Ville-Marie\tTémiscamingue\tAbitibi-Témiscamingue\n' +
    '      Warwick\tArthabaska\tCentre-du-Québec\n' +
    '      Waterloo\tLa Haute-Yamaska\tEstrie\n' +
    '      Waterville\tCoaticook\tEstrie\n' +
    '      Westmount\tÎle de Montréal\tAgglomération de Montréal\n' +
    '      Windsor\tLe Val-Saint-François\tEstrie'
  let arr = s.split('\n')
  arr.forEach(element => {
    let arr1 = element.split('\t')
    let ville = arr1[0]
    let bureau = arr1[1]
    let categorie = arr1[2]
    firestore.collection('config').doc('configNominisResidentielle').collection('cities').doc().set({
      city: ville,
      office: bureau,
      category: categorie
    })
  })
}
*/
