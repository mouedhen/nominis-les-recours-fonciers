import AbstractClass from '../../core/abstract-class'

class Owner extends AbstractClass {
  constructor ({category, name}) {
    super()
    this.category = category
    this.name = name
  }
}

export default Owner
