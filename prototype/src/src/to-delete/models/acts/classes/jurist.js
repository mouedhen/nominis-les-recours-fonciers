import AbstractClass from '../../core/abstract-class'

class Jurist extends AbstractClass {
  constructor ({category, name}) {
    super()
    this.category = category
    this.name = name
  }
}

export default Jurist
