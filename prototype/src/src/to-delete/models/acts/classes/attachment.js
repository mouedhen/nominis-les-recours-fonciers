import AbstractClass from '../../core/abstract-class'

class Attachment extends AbstractClass {
  constructor ({file}) {
    super()
    this.file = file
  }

  setFile ({name, type, url}) {
    this.file = {name, type, url}
  }
}

export default Attachment
