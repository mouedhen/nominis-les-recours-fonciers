import firestore from '../../../firestore'
import { cleanObject, convertTimestamp2String } from '../core/helpers'

import AbstractModel from '../core/abstract-model'
import { actStatus } from './enum/acts-status'

// const lot = {
//   id,
//   lotNumber,
//   act: {
//     attachment,
//     inscriptionNumber,
//     transactionType,
//     deadline,
//     owner: { category, name },
//     seller: { category, name },
//     jurist: { category, name }
//   },
//   office,
//   category,
//   address: { address, city, district, zipCode },
//   constructionYear,
//   fieldArea,
//   buildingArea,
//   floorsNumber,
//   housingsNumber,
//   neighborhoodUnitNumber,
//   landValue,
//   constructionValue,
//   buildingValue,
//   value,
//   img,
//   comment,
//   geoPoint: { latitude, longitude },
//   status,
//   history: [{action, date, user}]
// }

/**
 * @deprecated
 */
class Act extends AbstractModel {
  constructor (
    {
      id,
      // basic info
      attachment, inscriptionNumber, inscriptionDate, transactionType, deadline,
      // administrative info
      category, city, office, registrationNumber, folderNumber, neighborhoodUnitNumber,
      owner, seller, jurist,
      // addresses info
      addresses,
      // Characteristics info
      constructionYear, fieldArea, buildingArea, floorsNumber, housingsNumber, commercialPremisesNumber,
      // Values info
      landValue, constructionValue, buildingValue, value,
      // others
      img, comment, mapUrl, geoPoint,
      // management proprieties
      status, createdBy, createdAt, startProcessAt, processedBy, processedAt, correctedBy, startCorrectionAt,
      correctedAt, verifiedBy, verifiedAt, publishedBy, publishedAt
    }) {
    super()
    this.serialize({
      id,
      attachment,
      inscriptionNumber,
      inscriptionDate,
      transactionType,
      deadline,
      category,
      city,
      office,
      registrationNumber,
      folderNumber,
      neighborhoodUnitNumber,
      owner,
      seller,
      jurist,
      addresses,
      constructionYear,
      fieldArea,
      buildingArea,
      floorsNumber,
      housingsNumber,
      commercialPremisesNumber,
      landValue,
      constructionValue,
      buildingValue,
      value,
      img,
      comment,
      mapUrl,
      geoPoint,
      status,
      createdBy,
      createdAt,
      startProcessAt,
      processedBy,
      processedAt,
      correctedBy,
      startCorrectionAt,
      correctedAt,
      verifiedBy,
      verifiedAt,
      publishedBy,
      publishedAt
    })
    this.config = {
      collection: firestore.collection('acts'),
      archiveCollection: firestore.collection('archives').doc('nominisArchives').collection('acts')
    }
    this.setDataForDisplay()
  }

  serialize (
    {
      id,
      // basic info
      attachment, inscriptionNumber, inscriptionDate, transactionType, deadline,
      // administrative info
      category, city, office, registrationNumber, folderNumber, neighborhoodUnitNumber,
      owner, seller, jurist,
      // addresses info
      addresses,
      // Characteristics info
      constructionYear, fieldArea, buildingArea, floorsNumber, housingsNumber, commercialPremisesNumber,
      // Values info
      landValue, constructionValue, buildingValue, value,
      // others
      img, comment, mapUrl, geoPoint,
      // management proprieties
      status, createdBy, createdAt, startProcessAt, processedBy, processedAt, correctedBy, startCorrectionAt,
      correctedAt, verifiedBy, verifiedAt, publishedBy, publishedAt
    }) {
    this.id = id
    this.attachment = attachment
    this.inscriptionNumber = inscriptionNumber
    this.inscriptionDate = inscriptionDate
    this.transactionType = transactionType
    this.deadline = deadline

    this.category = category
    this.city = city
    this.office = office
    this.registrationNumber = registrationNumber
    this.folderNumber = folderNumber
    this.neighborhoodUnitNumber = neighborhoodUnitNumber
    this.owner = owner
    this.seller = seller
    this.jurist = jurist

    this.addresses = addresses

    this.constructionYear = constructionYear
    this.fieldArea = fieldArea
    this.buildingArea = buildingArea
    this.floorsNumber = floorsNumber
    this.housingsNumber = housingsNumber
    this.commercialPremisesNumber = commercialPremisesNumber

    this.landValue = landValue
    this.constructionValue = constructionValue
    this.buildingValue = buildingValue
    this.value = value

    this.img = img
    this.comment = comment
    this.mapUrl = mapUrl
    this.geoPoint = geoPoint

    this.status = status
    this.createdBy = createdBy
    this.createdAt = createdAt
    this.startProcessAt = startProcessAt
    this.processedBy = processedBy
    this.processedAt = processedAt
    this.correctedBy = correctedBy
    this.startCorrectionAt = startCorrectionAt
    this.correctedAt = correctedAt
    this.verifiedBy = verifiedBy
    this.verifiedAt = verifiedAt
    this.publishedBy = publishedBy
    this.publishedAt = publishedAt
  }

  setDataForDisplay () {
    if (this.inscriptionDate) {
      this.inscriptionDateDisplay = convertTimestamp2String(this.inscriptionDate)
    }
    if (this.deadline) {
      this.deadlineDisplay = convertTimestamp2String(this.deadline)
    }
    if (this.owner) {
      if (this.owner.category) {
        this.ownerDisplay = this.owner.category
      }
      if (this.owner.name) {
        if (this.owner.category) {
          this.ownerDisplay += ' - ' + this.owner.name
        } else {
          this.ownerDisplay += this.owner.name
        }
      }
    }
    if (this.seller) {
      if (this.seller.category) {
        this.sellerDisplay = this.seller.category
      }
      if (this.seller.name) {
        if (this.seller.category) {
          this.sellerDisplay += ' - ' + this.seller.name
        } else {
          this.sellerDisplay += this.seller.name
        }
      }
    }
    if (this.jurist) {
      if (this.jurist.category) {
        this.juristDisplay = this.jurist.category
      }
      if (this.jurist.name) {
        if (this.jurist.category) {
          this.juristDisplay += ' - ' + this.jurist.name
        } else {
          this.juristDisplay += this.jurist.name
        }
      }
    }
    if (this.addresses) {
      this.addressesDisplay = ''
      var i = 0
      this.addresses.forEach(address => {
        i++
        if (address.address) {
          if (address.address !== '') {
            this.addressesDisplay += address.address + '. '
          }
        }
        if (address.district) {
          if (address.district !== '') {
            this.addressesDisplay += address.district + '. '
          }
        }
        if (address.zipCode) {
          if (address.zipCode !== '') {
            this.addressesDisplay += address.zipCode + '. '
          }
        }
        if (address.lotNumber) {
          if (address.lotNumber !== '') {
            this.addressesDisplay += 'Lot n° ' + address.lotNumber + ' - '
          }
        }
        if (address.categories) {
          if (address.categories.length > 0) {
            this.addressesDisplay += 'Catégories : ' + address.categories.join(', ')
            if (i !== this.addresses.length) {
              this.addressesDisplay += ' || '
            }
          } else {
            if (this.addressesDisplay !== '') {
              if (i !== this.addresses.length) {
                this.addressesDisplay += ' || '
              }
            }
          }
        } else {
          if (this.addressesDisplay !== '') {
            if (i !== this.addresses.length) {
              this.addressesDisplay += ' || '
            }
          }
        }
      })
    }
  }

  setAddresses (addresses) {
    this.addresses = addresses.map(address => address.clean())
  }

  setOwner (owner) {
    this.owner = owner.clean()
  }

  setSeller (seller) {
    this.seller = seller.clean()
  }

  setJurist (jurist) {
    this.jurist = jurist.clean()
  }

  setImg (img) {
    this.img = img.clean()
  }

  setGeoPoint (geoPoint) {
    this.geoPoint = geoPoint
  }

  async createNew ({attachment, user}) {
    this.attachment = cleanObject(attachment)
    this.createdBy = user
    this.createdAt = new Date()
    this.status = actStatus.NEW
    return this.save()
  }

  async startProcess ({user}) {
    this.startProcessAt = new Date()
    this.processedBy = user
    this.status = actStatus.BEING_PROCESSED
    return this.save()
  }

  async endProcess () {
    this.processedAt = new Date()
    this.status = actStatus.PROCESSED
    return this.save()
  }

  async cancelProcess () {
    this.startProcessAt = null
    this.processedBy = null
    this.status = actStatus.NEW
    return this.save()
  }

  async startCorrection ({user}) {
    this.correctedBy = user
    this.startCorrectionAt = new Date()
    this.status = actStatus.BEING_CORRECTED
    return this.save()
  }

  async endCorrection () {
    this.correctedAt = new Date()
    this.status = actStatus.CORRECTED
    return this.save()
  }

  async cancelCorrection () {
    this.startCorrectionAt = null
    this.correctedBy = null
    this.status = actStatus.PROCESSED
    return this.save()
  }

  async verify ({user}) {
    this.verifiedBy = user
    this.verifiedAt = new Date()
    this.status = actStatus.VERIFIED
    return this.save()
  }

  async publish ({user}) {
    this.publishedBy = user
    this.publishedAt = new Date()
    this.status = actStatus.PUBLISHED
    return this.save()
  }
}

export default Act
