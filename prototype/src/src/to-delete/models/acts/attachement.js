import firestore from '../../../firestore'
import { docStatus } from '../core/helpers'
import AbstractModel from '../core/abstract-model'

/**
 * @deprecated
 */
class Attachment extends AbstractModel {
  constructor ({id, title, file, status, createdBy}) {
    super()
    this.serialize({id, title, file, status, createdBy})
    this.config = {collection: firestore.collection('attachments')}
  }

  serialize ({id, title, file, status, createdBy}) {
    this.id = id
    this.title = title
    this.file = file
    this.status = status
    this.timestamp = new Date()
    this.createdBy = createdBy
  }

  setFile ({name, type, url}) {
    this.file = {name, type, url}
  }

  async filterByStatus (status) {
    return this.fetchQuery([{params: 'status', operation: '==', value: status}])
  }

  async create () {
    this.status = docStatus.NEW
    return super.create()
  }

  async lock ({user}) {
    this.status = docStatus.BEING_PROCESSED
    this.processedBy = user
    this.beingProcessedAt = new Date()
    return this.update()
  }

  async finish () {
    this.status = docStatus.PROCESSED
    this.processedAt = new Date()
    return this.update()
  }
}

export default Attachment
