export const actStatus = {
  NEW: 'nouveau',
  BEING_PROCESSED: 'en cours de traitement',
  SAVED: 'sauvegardé',
  PROCESSED: 'traité',
  BEING_CORRECTED: 'correction en cours',
  CORRECTED: 'corrigé',
  VERIFIED: 'vérifié',
  PUBLISHED: 'publié',
  ARCHIVED: 'archivé'
}
