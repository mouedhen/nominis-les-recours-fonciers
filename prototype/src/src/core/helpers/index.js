import moment from 'moment-timezone'

moment.locale('fr')

/**
 * Clean an object form all undefined properties
 * @param obj
 * @returns {{}}
 */
export function cleanObject (obj) {
  return Object.keys(obj).filter(key => obj[key]).reduce(
    (newObj, key) => {
      // console.log('key =', key)
      if (key !== 'config' &&
        key !== 'password' &&
        key !== 'passwordConfirm' &&
        key !== 'deadlineDisplay' &&
        key !== 'inscriptionDateDisplay' &&
        key !== 'juristDisplay' &&
        key !== 'ownerDisplay' &&
        key !== 'sellerDisplay' &&
        key !== 'processedBy' &&
        key !== 'correctedBy' &&
        key !== 'publishedBy' &&
        key !== 'beingProcessedBy' &&
        key !== 'beingCorrectedBy' &&
        key !== 'processor' &&
        key !== 'corrector' &&
        key !== 'publisher') {
        newObj[key] = obj[key]
      }
      return newObj
    }, {}
  )
}

/**
 * Convert timestamp to string
 * @param date
 * @returns {*}
 */
export function convertTimestamp2String (date) {
  if (typeof date === 'string') return date
  if (date) {
    let timestamp = date.seconds
    // return moment.unix(timestamp).utc().calendar()
    return moment.utc(moment.unix(timestamp)).format('DD/MM/YYYY')
  }
}

/**
 * Convert timestamp to string
 * @param date
 * @returns {*}
 */
export function convertTimestamp2DateTimeString (date) {
  if (typeof date === 'string') return date
  if (date) {
    let timestamp = date.seconds
    return moment.utc(moment.unix(timestamp)).format('LLLL')
  }
}

/**
 * Convert timestamp to string
 * @param date
 * @returns {*}
 */
export function convertTimestamp2DateString (date) {
  if (typeof date === 'string') return date
  if (date) {
    let timestamp = date.seconds
    // return moment.unix(timestamp).utc().calendar()
    // return moment.unix(timestamp).utc().format('DD/MM/YYYY')
    return moment.utc(moment.unix(timestamp)).format('DD/MM/YYYY')
  }
}

/**
 * Check if the passed string contain the passed value
 * @param {string} value
 * @param {string} search
 * @returns {boolean}
 */
export function hadString (value, search) {
  return value !== undefined &&
    value !== null &&
    value.toString().toLowerCase().normalize().includes(search.toLowerCase().normalize())
}

/**
 * Check if the passed string array contain the passed value
 * @param values
 * @param {string} search
 * @returns {boolean}
 */
export function arrayHadString (values = [], search = '') {
  let result = false
  values.forEach(v => {
    result = hadString(v, search) || result
  })
  return result
}

function s2ab (s) {
  const buf = new ArrayBuffer(s.length)
  const view = new Uint8Array(buf)
  for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF
  return buf
}

export function downloadModelUsExls (collection, fileName) {
  let json2xls = require('json2xls')
  let xls = json2xls(collection)
  let FileSaver = require('file-saver')
  const blob = new Blob([s2ab(xls)], {
    type: 'application/vnd.ms-excel'
  })
  FileSaver.saveAs(blob, fileName + '.xls')
}

export function arrayToString (value) {
  if (!value && !Array.isArray(value)) return 'N/D'
  if (value.length === 0) return 'N/D'
  let result = ''
  value.forEach(v => {
    result += v + ', '
  })
  return result
}

export function notDefined (value) {
  if (value === undefined || value === null || value === 0 || value === 'non déclarè' || value === 'non disponible') return 'N/D'
  return value.toString()
}

export function notDefinedCurrency (value) {
  if (value === undefined || value === null || value === 0 || value === 'non déclarè' || value === 'non disponible') return 'N/D'
  return parseFloat(value).toLocaleString('fr') + ' .$'
}
