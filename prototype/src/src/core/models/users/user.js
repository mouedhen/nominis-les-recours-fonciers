import firebase from '../../../firebase'
import firestore from '../../../firestore'
import 'firebase/functions'

import { convertTimestamp2String, cleanObject } from '../../helpers'

const usersCollection = firestore.collection('users')

class User {
  constructor ({firstName, lastName, phoneNumber, email, password, passwordConfirm, role, isActive, uid, createdAt, updatedAt}) {
    this.serialize({
      firstName,
      lastName,
      phoneNumber,
      email,
      password,
      passwordConfirm,
      role,
      isActive,
      uid,
      createdAt,
      updatedAt
    })
    if (this.createdAt) {
      this.createdAt = convertTimestamp2String(this.createdAt)
    }
    if (this.updatedAt) {
      this.updatedAt = convertTimestamp2String(this.updatedAt)
    }
  }

  clean () {
    return cleanObject(this)
  }

  serialize ({firstName, lastName, phoneNumber, email, password, passwordConfirm, role, isActive, uid, createdAt, updatedAt}) {
    this.firstName = firstName !== undefined ? firstName : null
    this.lastName = lastName !== undefined ? lastName : null
    this.phoneNumber = phoneNumber !== undefined ? phoneNumber : null
    this.email = email !== undefined ? email : null
    this.password = password !== undefined ? password : null
    this.passwordConfirm = passwordConfirm !== undefined ? passwordConfirm : null
    this.role = role !== undefined ? role : null
    this.isActive = isActive !== undefined ? isActive : false
    this.uid = uid !== undefined ? uid : null
    this.createdAt = createdAt !== undefined ? createdAt : null
    this.updatedAt = updatedAt !== undefined ? updatedAt : null
    return this
  }

  async createUser (email, password, role, isActive) {
    let createUser = firebase.functions().httpsCallable('createUser')
    return createUser({email: email, password: password, isActive: !isActive, role: role}).then(function (result) {
      return (result.data.uid)
    })
  }

  async updateUser (email, role, isActive, uid) {
    let updateUser = firebase.functions().httpsCallable('updateUser')
    return updateUser({email: email, isActive: !isActive, role: role, uid: uid})
  }

  async updateIsActive () {
    let updateUser = firebase.functions().httpsCallable('updateUser')
    return updateUser({isActive: !this.isActive, uid: this.uid}).then(() => {
      usersCollection.doc(this.uid).update({
        isActive: this.isActive,
        updatedAt: new Date()
      })
    })
  }

  async deleteUser (uid) {
    let deleteUser = firebase.functions().httpsCallable('deleteUser')
    return deleteUser({uid: uid})
  }

  isActiveUser () {
    return this.isActive !== undefined && this.isActive !== null && this.isActive !== false
  }

  async create () {
    let data = {
      ...this.clean(),
      createdAt: new Date(),
      updatedAt: new Date()
    }
    this.isActive = data.isActive = this.isActiveUser()
    return this.createUser(this.email, this.password, this.role, this.isActive).then(uid => {
      this.uid = uid
      let newUserRef = usersCollection.doc(uid)
      return newUserRef.set(data)
    })
  }

  async update () {
    let data = {
      ...this.clean(),
      updatedAt: new Date()
    }
    this.isActive = data.isActive = this.isActiveUser()
    return this.updateUser(this.email, this.role, this.isActive, this.uid).then(uid => {
      return usersCollection.doc(this.uid).update(data)
    })
  }

  async remove () {
    return this.deleteUser(this.uid).then(() => {
      return usersCollection.doc(this.uid).delete()
    })
  }

  async signInWithEmail (email) {
    return firebase.auth().signInWithEmailAndPassword(email, this.password)
  }

  async signOut () {
    return firebase.auth().signOut()
  }

  async resetPassword (email) {
    return firebase.auth().sendPasswordResetEmail(email)
  }

  async getUserById () {
    return usersCollection.doc(this.uid).get().then(doc => {
      if (doc.exists) {
        return this.serialize({...doc.data(), uid: doc.id})
      } else {
        throw new Error('need to commit customer')
      }
    })
  }

  async fetchAll () {
    return usersCollection.get().then(snapshot => {
      let users = []
      snapshot.forEach(doc => {
        users.push(new User({uid: doc.id, ...doc.data()}))
      })
      return users
    })
  }

  async setActive (isActive) {
    this.isActive = isActive
    return this.updateIsActive()
  }

  async getProfile () {
    if (firebase.auth().currentUser === null) return this.serialize({})
    this.uid = firebase.auth().currentUser.uid
    return this.getUserById(firebase.auth().currentUser.uid)
  }
}

export default User
