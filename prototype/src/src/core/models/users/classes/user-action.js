import AbstractClass from '../../core/abstract-class'

class UserAction extends AbstractClass {
  constructor ({action, date, user}) {
    super({action, date, user})
  }

  serialize ({action, date, user}) {
    this.action = action !== undefined ? action : null
    this.date = date !== undefined ? date : null
    this.user = user !== undefined ? user : null
    return this
  }
}

export default UserAction
