export const roles = {
  VERIFICATION_OFFICIER: {
    label: 'verification_officier',
    designation: 'Agent de vérification'
  },
  ENTRY_OFFICIER: {
    label: 'entry_officier',
    designation: 'Agent de saisie'
  },
  ADMINISTRATOR: {
    label: 'administrator',
    designation: 'Administrateur'
  }
}
