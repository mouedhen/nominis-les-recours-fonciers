import moment from 'moment-timezone'

import firestore from '../../../firestore'
import AbstractModel from '../core/abstract-model'
import { convertTimestamp2String } from '../../helpers'
import {newsCollection} from '../../../to-delete/models/core/helpers'
moment.locale('fr')

class News extends AbstractModel {
  constructor ({type, title, body, isPublished, createdAt, updatedAt, publishedAt, id}) {
    super({type, title, body, isPublished, createdAt, updatedAt, publishedAt, id})
    this.config = {
      collection: firestore.collection('news'),
      archiveCollection: firestore.collection('archives').doc(moment().format('LL')).collection('news')
    }
    this.serialize({type, title, body, isPublished, createdAt, updatedAt, publishedAt, id})
    this.createdAt = convertTimestamp2String(createdAt)
    this.updatedAt = convertTimestamp2String(updatedAt)
    this.publishedAt = convertTimestamp2String(publishedAt)
  }

  serialize ({type, title, body, isPublished, createdAt, updatedAt, publishedAt, id}) {
    this.id = id !== undefined ? id : null
    this.type = type !== undefined ? type : null
    this.title = title !== undefined ? title : null
    this.body = body !== undefined ? body : null
    this.isPublished = isPublished !== undefined ? isPublished : false
    this.createdAt = createdAt !== undefined ? createdAt : null
    this.updatedAt = updatedAt !== undefined ? updatedAt : null
    this.publishedAt = publishedAt !== undefined ? publishedAt : null
    return this
  }

  async create () {
    this.isPublished = this.isPublished !== undefined && this.isPublished !== null && this.isPublished !== false
    this.createdAt = new Date()
    this.updatedAt = new Date()
    this.publishedAt = this.isPublished ? new Date() : null
    return newsCollection.doc(this.id).set({...this.clean()})
  }

  async update () {
    this.updatedAt = new Date()
    this.publishedAt = this.isPublished ? new Date() : null
    return super.update()
  }

  async remove () {
    return newsCollection.doc(this.id).delete()
  }

  async getLastTenNewsArray () {
    let query = this.queryBuilder([{
      params: 'isPublished',
      operation: '==',
      value: true
    }]).orderBy('publishedAt', 'desc').limit(10)
    return this.fetchQuery(query)
  }

  async setPublished (isPublished) {
    this.isPublished = isPublished
    this.updatedAt = new Date()
    this.publishedAt = isPublished ? new Date() : null
    return this.update()
  }

  async getIsPublished () {
    return this.fetch(this.id)
      .then(news => {
        return news.isPublished
      })
  }

  async getNewsByID () {
    return this.config.collection.doc(this.id).get().then(document => {
      let val = document.data()
      if (val) {
        this.body = val.body
        this.title = val.title
        this.type = val.type
        this.isPublished = val.isPublished
        this.createdAt = val.createdAt
        this.updatedAt = val.updatedAt
        this.publishedAt = val.publishedAt
      }
      return this
    })
  }
}

export default News
