export const actStatus = {
  NEW: 'nouveau', // = en attente de traitement
  BEING_PROCESSED: 'en cours de traitement',
  PROCESSED: 'en attente de vérification', // = traité
  BEING_CORRECTED: 'en cours de vérification',
  CORRECTED: 'en attente de publication', // = vérifié
  RETURNED: 'remis à la saisie', // = en attente de (re)traitement
  PUBLISHED: 'publié'// ,
  // ARCHIVED: 'archivé'
}

export const actStatusEntry = {
  NEW: 'nouveau', // = en attente de traitement
  BEING_PROCESSED: 'en cours de traitement',
  RETURNED: 'remis à la saisie' // = en attente de (re)traitement
}

export const actStatusVerification = {
  NEW: 'nouveau', // = en attente de traitement
  BEING_PROCESSED: 'en cours de traitement',
  PROCESSED: 'en attente de vérification', // = traité
  BEING_CORRECTED: 'en cours de vérification',
  CORRECTED: 'en attente de publication', // = vérifié
  RETURNED: 'remis à la saisie' // = en attente de (re)traitement
}

export const actStatusPublication = {
  CORRECTED: 'en attente de publication', // = vérifié
  PUBLISHED: 'publié'// ,
  // ARCHIVED: 'archivé'
}

export const actActions = {
  NEW: 'a créé l\'acte',
  BEING_PROCESSED: 'a commencé la saisie de l\'acte',
  PROCESSING_CANCELED: 'a annulé la saisie de l\'acte',
  PROCESSED: 'a terminé la saisie de l\'acte',
  BEING_CORRECTED: 'a commencé la correction de l\'acte',
  CORRECTION_CANCELED: 'a annulé la correction de l\'acte',
  CORRECTED: 'a terminé la correction de l\'acte',
  RETURNED: 'a remis à la saisie l\'acte',
  PUBLISHED: 'a publié l\'acte'// ,
  // ARCHIVED: 'archivage'
}
