import AbstractClass from '../../core/abstract-class'

class Owner extends AbstractClass {
  constructor ({name, email, phone, facebook, twitter, linkedin, category}) {
    super({name, email, phone, facebook, twitter, linkedin, category})
  }

  serialize ({name, email, phone, facebook, twitter, linkedin, category}) {
    this.name = name !== undefined ? name : null
    this.email = email !== undefined ? email : null
    this.phone = phone !== undefined ? phone : null
    this.facebook = facebook !== undefined ? facebook : null
    this.twitter = twitter !== undefined ? facebook : null
    this.linkedin = linkedin !== undefined ? linkedin : null
    this.category = category !== undefined ? category : null
  }
}

export default Owner
