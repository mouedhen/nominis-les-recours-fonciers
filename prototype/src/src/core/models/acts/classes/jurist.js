import AbstractClass from '../../core/abstract-class'

class Jurist extends AbstractClass {
  constructor ({name, category}) {
    super({name, category})
  }

  serialize ({name, category}) {
    this.name = name !== undefined ? name : null
    this.category = category !== undefined ? category : null
  }
}

export default Jurist
