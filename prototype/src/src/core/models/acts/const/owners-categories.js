export const ownersCategories = [
  'acquéreur',
  'cédant',
  'cessionnaire',
  'débiteur',
  'débiteur / propriétaire',
  'donataire',
  'emphytéote',
  'failli',
  'locataire',
  'propriétaire',
  'requérant',
  'subrogé'
]
