import AbstractClass from '../../core/abstract-class'

class TransactionType extends AbstractClass {
  constructor ({id, type}) {
    super({id, type})
  }

  serialize ({id, type}) {
    this.id = id !== undefined ? id : null
    this.type = type !== undefined ? type : null
  }
}

export default TransactionType
