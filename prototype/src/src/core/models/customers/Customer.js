import firestore from '../../../firestore'
import { cleanObject, convertTimestamp2String } from '../../helpers'
import firebase from '../../../firebase'

const customersCollection = firestore.collection('customers')
const purchasesCollection = firestore.collection('purchases')

/**
 * flat customer
 * @param {Customer} customer
 * @returns {{}}
 */
export function flatCustomer (customer) {
  return {
    'Prénom': customer.firstName,
    'Nom': customer.lastName,
    'Courriel': customer.email,
    'Téléphone': customer.phoneNumber,
    'Abonnement': customer.membership,
    'Créer le': customer.createdAt,
    'Est activé': customer.isActive
  }
}

class Customer {
  constructor (
    {
      id,
      lastName,
      firstName,
      email,
      phoneNumber,
      // credits,
      membership,
      isActive,
      password,
      passwordConfirm,
      createdAt,
      updatedAt
    }) {
    this.serialize({
      id,
      lastName,
      firstName,
      email,
      phoneNumber,
      // credits,
      membership,
      isActive,
      password,
      passwordConfirm,
      createdAt,
      updatedAt
    })
    if (this.createdAt) {
      this.createdAt = convertTimestamp2String(this.createdAt)
    }
    if (this.updatedAt) {
      this.updatedAt = convertTimestamp2String(this.updatedAt)
    }
  }

  serialize ({id, lastName, firstName, email, phoneNumber/* , credits */, membership, isActive, password, passwordConfirm, createdAt, updatedAt}) {
    this.id = id !== undefined ? id : null
    this.firstName = firstName !== undefined ? firstName : null
    this.lastName = lastName !== undefined ? lastName : null
    this.phoneNumber = phoneNumber !== undefined ? phoneNumber : null
    this.email = email !== undefined ? email : null
    this.password = password !== undefined ? password : null
    this.passwordConfirm = passwordConfirm !== undefined ? passwordConfirm : null
    this.isActive = isActive !== undefined ? isActive : false
    this.createdAt = createdAt !== undefined ? createdAt : null
    this.updatedAt = updatedAt !== undefined ? updatedAt : null
    // this.credits = credits !== undefined ? credits : null
    this.membership = membership !== undefined ? membership : null
    return this
  }

  async createCustomer (email, password, isActive) {
    let createCustomer = firebase.functions().httpsCallable('createCustomer')
    return createCustomer({email: email, password: password, isActive: !isActive}).then(function (result) {
      return (result.data.uid)
    })
  }

  async updateCustomer (isActive, uid) {
    let updateCustomer = firebase.functions().httpsCallable('updateCustomer')
    return updateCustomer({isActive: !isActive, uid: uid})
  }

  async deleteUser (uid) {
    let deleteUser = firebase.functions().httpsCallable('deleteUser')
    return deleteUser({uid: uid})
  }

  async buyAct (lot) {
    // @TODO use bus event, use firebase transactions
    if (parseInt(this.credits) > 0) {
      return purchasesCollection.doc().set({lotId: lot.id, userId: this.id, boughtAt: new Date()}).then(() => {
        return customersCollection.doc(this.id).update({
          credits: '' + (parseInt(this.credits) - 1)
        }).then(() => {
          // add history to lots
          this.credits = '' + (parseInt(this.credits) - 1)
        })
      })
    } else {
      throw new Error('Insufficient credits')
    }
  }

  isActiveCustomer () {
    return this.isActive !== undefined && this.isActive !== null && this.isActive !== false
  }

  async create () {
    let data = {
      ...this.clean(),
      createdAt: new Date(),
      updatedAt: new Date()
    }
    this.isActive = data.isActive = this.isActiveCustomer()
    return this.createCustomer(this.email, this.password, this.isActive).then(uid => {
      this.uid = uid
      let newCustomerRef = customersCollection.doc(uid)
      return newCustomerRef.set(data)
    })
  }

  async updateIsActive () {
    let updateCustomer = firebase.functions().httpsCallable('updateCustomer')
    return updateCustomer({isActive: !this.isActive, uid: this.id}).then(() => {
      customersCollection.doc(this.id).update({
        isActive: this.isActive,
        updatedAt: new Date()
      })
    })
  }

  async update () {
    let data = {
      ...this.clean(),
      updatedAt: new Date()
    }
    this.isActive = data.isActive = this.isActiveCustomer()
    return this.updateCustomer(this.isActive, this.id).then(_ => {
      return customersCollection.doc(this.id).update(data)
    })
  }

  async remove () {
    return this.deleteUser(this.id).then(() => {
      return customersCollection.doc(this.id).delete()
    })
  }

  async getCustomerById () {
    return customersCollection.doc(this.id).get().then(doc => {
      return this.serialize({...doc.data(), id: doc.id})
    })
  }

  async fetchAll () {
    return customersCollection.get().then(snapshot => {
      let customers = []
      snapshot.forEach(doc => {
        customers.push(new Customer({id: doc.id, ...doc.data()}))
      })
      return customers
    })
  }

  async setActive (isActive) {
    this.isActive = isActive
    return this.updateIsActive()
  }

  async getProfile () {
    if (firebase.auth().currentUser === null) return this.serialize({})
    this.id = firebase.auth().currentUser.uid
    return this.getCustomerById(this.id)
  }

  clean () {
    return cleanObject(this)
  }
}

export default Customer
