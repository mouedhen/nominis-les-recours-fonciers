import firestore from '../../../firestore'
import AbstractModel from '../core/abstract-model'

class Favorite extends AbstractModel {
  constructor ({id, userId, lotId}) {
    super({id, userId, lotId})
    this.config = {
      collection: firestore.collection('favorites')
    }
  }

  serialize ({id, userId, lotId}) {
    this.id = id !== undefined ? id : null
    this.userId = userId !== undefined ? userId : null
    this.lotId = lotId !== undefined ? lotId : null
    return this
  }
}

export default Favorite
