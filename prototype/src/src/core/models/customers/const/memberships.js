export const memberships = {
  BASIC: {
    label: 'basic',
    designation: 'Basique'
  },
  PREMIUM: {
    label: 'premium',
    designation: 'Premium'
  },
  VIP: {
    label: 'vip',
    designation: 'VIP'
  }
}
