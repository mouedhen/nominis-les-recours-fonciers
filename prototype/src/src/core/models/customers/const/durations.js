export const durations = {
  TODAY: {
    label: 0,
    designation: 'Aujourd\'hui'
  },
  LAST_3_DAYS: {
    label: 3,
    designation: '3 derniers jours'
  },
  LAST_7_DAYS: {
    label: 7,
    designation: '7 derniers jours'
  },
  LAST_15_DAYS: {
    label: 15,
    designation: '15 derniers jours'
  },
  LAST_30_DAYS: {
    label: 30,
    designation: '30 derniers jours'
  },
  LAST_60_DAYS: {
    label: 60,
    designation: '60 derniers jours'
  }
}
