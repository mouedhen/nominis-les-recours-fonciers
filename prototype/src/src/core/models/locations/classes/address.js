import AbstractClass from '../../core/abstract-class'

class Address extends AbstractClass {
  constructor ({address, address2, district, city, zipCode, categories, zoningType}) {
    super({address, address2, district, city, zipCode, categories, zoningType})
  }

  serialize ({address, address2, district, city, zipCode, zoningType, categories}) {
    this.address = address !== undefined ? address : null
    this.address2 = address2 !== undefined ? address2 : null
    this.district = district !== undefined ? district : null
    this.city = city !== undefined ? city : null
    this.zipCode = zipCode !== undefined ? zipCode : null
    this.categories = categories !== undefined ? categories : []
    this.zoningType = zoningType !== undefined ? zoningType : null
    return this
  }
}

export default Address
