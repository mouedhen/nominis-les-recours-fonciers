import firebase from '@/firebase'
import 'firebase/storage'

class Document {
  constructor ({file}) {
    this.file = file !== undefined ? file : null
    this.name = name !== undefined ? (+new Date()) + file.name : null
    this.metadata = {contentType: file.type}
    this.ref = firebase.storage().ref()
    this.task = this.ref.child(this.name).put(this.file, this.metadata)
  }

  async uploadFile () {
    return this.task.then((snapshot) => {
      return snapshot.ref.getDownloadURL()
        .then(url => {
          return {
            name: snapshot.metadata.fullPath,
            type: snapshot.metadata.contentType,
            url
          }
        })
    })
  }

  static async deleteFile (name) {
    return firebase.storage().ref().child(name).delete()
  }
}

export default Document
