import AbstractClass from '../../core/abstract-class'

class Attachment extends AbstractClass {
  constructor ({name, type, url}) {
    super({name, type, url})
  }

  serialize ({name, type, url}) {
    this.name = name !== undefined ? name : null
    this.type = type !== undefined ? type : null
    this.url = url !== undefined ? url : null
    return this
  }
}

export default Attachment
