import Vue from 'vue'
// import 'element-ui/lib/theme-chalk/index.css'
import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'

import './assets/scss/element-variables.scss'

import lang from 'element-ui/lib/locale/lang/fr'
import locale from 'element-ui/lib/locale'

import { DataTables } from 'vue-data-tables'

import 'swiper/dist/css/swiper.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import * as VueGoogleMaps from 'vue2-google-maps'

const VueInputMask = require('vue-inputmask').default
Vue.use(VueGoogleMaps, {load: {key: 'AIzaSyCd5vfKP6P5O6BTi1okd6WtHCfHurawkCg', libraries: 'places'}})
locale.use(lang)
Vue.use(ElementUI)
Vue.use(DataTables)
Vue.use(VueAwesomeSwiper)
Vue.use(VueInputMask)
