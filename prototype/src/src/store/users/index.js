import firebase from '../../firebase'
import firestore from '../../firestore'

import { LOGIN, LOGIN_CUSTOMER, LOGOUT, CREATE, DELETE, FETCH_ALL, INIT, UPDATE } from './mutations-types'
import { state } from './state'
import { mutations } from './mutations'
import { getters } from './getters'
import User from '../../core/models/users/user'
import Customer from '../../core/models/customers/Customer'

const actions = {
  async fetchAll (context) {
    (new User({})).fetchAll()
      .then(users => {
        context.commit(FETCH_ALL, users)
      })
      .catch(error => {
        console.log(error)
      })
  },

  async syncCollection (context) {
    context.commit(INIT)
    return firestore.collection('users').onSnapshot(doc => {
      doc.docChanges().forEach(snapshot => {
        switch (snapshot.type) {
          case 'added':
            return context.commit(CREATE, new User({uid: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'removed':
            return context.commit(DELETE, new User({uid: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'modified':
            if (snapshot.oldIndex !== snapshot.newIndex) {
              context.commit(DELETE, new User({uid: snapshot.oldIndex, ...snapshot.doc.data()}))
              return context.commit(CREATE, new User({uid: snapshot.newIndex, ...snapshot.doc.data()}))
            } else {
              return context.commit(UPDATE, new User({uid: snapshot.doc.id, ...snapshot.doc.data()}))
            }
        }
      })
    })
  },
  async syncAuthUser (context) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        return new User({}).getProfile()
          .then(user => {
            context.commit(LOGIN, user)
          }).catch(error => {
            if (error.message === 'need to commit customer') {
              return new Customer({}).getProfile().then(customer => {
                context.commit(LOGIN_CUSTOMER, customer)
              })
            } else {
              console.log(error)
              context.commit(LOGOUT)
            }
          })
      } else {
        context.commit(LOGOUT)
      }
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
