import User from '../../core/models/users/user'
import Customer from '../../core/models/customers/Customer'

export const state = {
  all: [],
  user: new User({}),
  customer: new Customer({})
}
