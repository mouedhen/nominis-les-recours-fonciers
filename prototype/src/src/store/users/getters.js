export const getters = {
  getAll: state => state.all,
  getCurrentUser: state => state.user,
  getCurrentCustomer: state => state.customer
}
