import Vue from 'vue'
import Vuex from 'vuex'

import users from './users'
import customers from './customers'
import favorites from './favorites'
import lots from './lots'
import publications from './publications'
import purchases from './purchases'
import news from './news'
import transactionType from './transactionType'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    users,
    lots,
    publications,
    purchases,
    news,
    favorites,
    transactionType,
    customers
  }
})
