import firestore from '../../firestore'
import Lot from '../../core/models/acts/lot'

import { INIT, FETCH_ALL, CREATE, UPDATE, DELETE, CREATE_VERIFICATION, UPDATE_VERIFICATION, DELETE_VERIFICATION, CREATE_ENTRY, UPDATE_ENTRY, DELETE_ENTRY, CREATE_PUBLICATION, UPDATE_PUBLICATION, DELETE_PUBLICATION } from './mutations-types'
import { state } from './state'
import { mutations } from './mutations'
import { getters } from './getters'
import { actStatus } from '../../core/models/acts/enum/acts-status'

const actions = {
  async fetchAll (context) {
    (new Lot({})).fetchAll()
      .then(acts => {
        context.commit(FETCH_ALL, acts)
      })
      .catch(error => {
        console.log(error)
      })
  },

  async syncCollection (context) {
    context.commit(INIT)
    return firestore.collection('lots').onSnapshot(doc => {
      doc.docChanges().forEach(snapshot => {
        switch (snapshot.type) {
          case 'added':
            if (snapshot.doc.data().status === actStatus.NEW || snapshot.doc.data().status === actStatus.BEING_PROCESSED || snapshot.doc.data().status === actStatus.RETURNED || snapshot.doc.data().status === actStatus.BEING_CORRECTED || snapshot.doc.data().status === actStatus.CORRECTED || snapshot.doc.data().status === actStatus.PROCESSED) {
              if (snapshot.doc.data().status === actStatus.NEW || snapshot.doc.data().status === actStatus.BEING_PROCESSED || snapshot.doc.data().status === actStatus.RETURNED) {
                context.commit(CREATE_ENTRY, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
              } else {
                context.commit(DELETE_ENTRY, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
              }
              context.commit(CREATE_VERIFICATION, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
            } else {
              context.commit(DELETE_VERIFICATION, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
              context.commit(DELETE_ENTRY, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
            }
            if (snapshot.doc.data().status === actStatus.CORRECTED || snapshot.doc.data().status === actStatus.PUBLISHED || snapshot.doc.data().status === actStatus.ARCHIVED) {
              context.commit(CREATE_PUBLICATION, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
            } else {
              context.commit(DELETE_PUBLICATION, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
            }
            return context.commit(CREATE, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'removed':
            context.commit(DELETE_PUBLICATION, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
            context.commit(DELETE_VERIFICATION, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
            context.commit(DELETE_ENTRY, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
            return context.commit(DELETE, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'modified':
            if (snapshot.oldIndex !== snapshot.newIndex) {
              if (snapshot.doc.data().status === actStatus.NEW || snapshot.doc.data().status === actStatus.BEING_PROCESSED || snapshot.doc.data().status === actStatus.RETURNED || snapshot.doc.data().status === actStatus.BEING_CORRECTED || snapshot.doc.data().status === actStatus.CORRECTED || snapshot.doc.data().status === actStatus.PROCESSED) {
                if (snapshot.doc.data().status === actStatus.NEW || snapshot.doc.data().status === actStatus.BEING_PROCESSED || snapshot.doc.data().status === actStatus.RETURNED) {
                  context.commit(DELETE_ENTRY, new Lot({id: snapshot.oldIndex, ...snapshot.doc.data()}))
                  context.commit(CREATE_ENTRY, new Lot({id: snapshot.newIndex, ...snapshot.doc.data()}))
                } else {
                  context.commit(DELETE_ENTRY, new Lot({id: snapshot.oldIndex, ...snapshot.doc.data()}))
                }
                context.commit(DELETE_VERIFICATION, new Lot({id: snapshot.oldIndex, ...snapshot.doc.data()}))
                context.commit(CREATE_VERIFICATION, new Lot({id: snapshot.newIndex, ...snapshot.doc.data()}))
              } else {
                context.commit(DELETE_VERIFICATION, new Lot({id: snapshot.oldIndex, ...snapshot.doc.data()}))
                context.commit(DELETE_ENTRY, new Lot({id: snapshot.oldIndex, ...snapshot.doc.data()}))
              }
              if (snapshot.doc.data().status === actStatus.CORRECTED || snapshot.doc.data().status === actStatus.PUBLISHED || snapshot.doc.data().status === actStatus.ARCHIVED) {
                context.commit(DELETE_PUBLICATION, new Lot({id: snapshot.oldIndex, ...snapshot.doc.data()}))
                context.commit(CREATE_PUBLICATION, new Lot({id: snapshot.newIndex, ...snapshot.doc.data()}))
              } else {
                context.commit(DELETE_PUBLICATION, new Lot({id: snapshot.oldIndex, ...snapshot.doc.data()}))
              }
              context.commit(DELETE, new Lot({id: snapshot.oldIndex, ...snapshot.doc.data()}))
              return context.commit(CREATE, new Lot({id: snapshot.newIndex, ...snapshot.doc.data()}))
            } else {
              if (snapshot.doc.data().status === actStatus.NEW || snapshot.doc.data().status === actStatus.BEING_PROCESSED || snapshot.doc.data().status === actStatus.RETURNED || snapshot.doc.data().status === actStatus.BEING_CORRECTED || snapshot.doc.data().status === actStatus.CORRECTED || snapshot.doc.data().status === actStatus.PROCESSED) {
                if (snapshot.doc.data().status === actStatus.NEW || snapshot.doc.data().status === actStatus.BEING_PROCESSED || snapshot.doc.data().status === actStatus.RETURNED) {
                  context.commit(UPDATE_ENTRY, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
                } else {
                  context.commit(DELETE_ENTRY, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
                }
                context.commit(UPDATE_VERIFICATION, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
              } else {
                context.commit(DELETE_ENTRY, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
                context.commit(DELETE_VERIFICATION, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
              }
              if (snapshot.doc.data().status === actStatus.CORRECTED || snapshot.doc.data().status === actStatus.PUBLISHED || snapshot.doc.data().status === actStatus.ARCHIVED) {
                context.commit(UPDATE_PUBLICATION, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
              } else {
                context.commit(DELETE_PUBLICATION, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
              }
              return context.commit(UPDATE, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
            }
        }
      })
    })
  },

  async syncPublishedCollection (context) {
    context.commit(INIT)
    return firestore.collection('lots').where('status', '==', actStatus.PUBLISHED).onSnapshot(doc => {
      doc.docChanges().forEach(snapshot => {
        switch (snapshot.type) {
          case 'added':
            return context.commit(CREATE, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'removed':
            return context.commit(DELETE, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'modified':
            if (snapshot.oldIndex !== snapshot.newIndex) {
              context.commit(DELETE, new Lot({id: snapshot.oldIndex, ...snapshot.doc.data()}))
              return context.commit(CREATE, new Lot({id: snapshot.newIndex, ...snapshot.doc.data()}))
            } else {
              return context.commit(UPDATE, new Lot({id: snapshot.doc.id, ...snapshot.doc.data()}))
            }
        }
      })
    })
  },

  async fetchQuery (context, filters) {
    (new Lot({})).fetchQuery(filters)
      .then(acts => {
        context.commit(FETCH_ALL, acts)
      })
      .catch(error => {
        console.log(error)
      })
  },

  async create (context) {
    (new Lot({})).create()
      .then(act => {
        context.commit(CREATE, act)
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
