export const getters = {
  getAll: state => state.all,
  getVerification: state => state.verification,
  getEntry: state => state.entry,
  getPublication: state => state.publication
}
