import firestore from '../../firestore'
import News from '../../core/models/news/news'

import { INIT, FETCH_ALL, CREATE, UPDATE, DELETE } from './mutations-types'
import { state } from './state'
import { mutations } from './mutations'
import { getters } from './getters'

const actions = {
  async fetchAll (context) {
    (new News({})).fetchAll()
      .then(news => {
        context.commit(FETCH_ALL, news)
      })
      .catch(error => {
        console.log(error)
      })
  },

  async syncCollection (context) {
    context.commit(INIT)
    return firestore.collection('news').onSnapshot(doc => {
      doc.docChanges().forEach(snapshot => {
        switch (snapshot.type) {
          case 'added':
            return context.commit(CREATE, new News({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'removed':
            return context.commit(DELETE, new News({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'modified':
            if (snapshot.oldIndex !== snapshot.newIndex) {
              context.commit(DELETE, new News({id: snapshot.oldIndex, ...snapshot.doc.data()}))
              return context.commit(CREATE, new News({id: snapshot.newIndex, ...snapshot.doc.data()}))
            } else {
              return context.commit(UPDATE, new News({id: snapshot.doc.id, ...snapshot.doc.data()}))
            }
        }
      })
    })
  },

  async fetchQuery (context, filters) {
    (new News({})).fetchQuery(filters)
      .then(news => {
        context.commit(FETCH_ALL, news)
      })
      .catch(error => {
        console.log(error)
      })
  },

  async create (context) {
    (new News({})).create()
      .then(news => {
        context.commit(CREATE, news)
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
