import {
  INIT,
  FETCH_ALL,
  FETCH,
  CREATE,
  UPDATE,
  DELETE
} from './mutations-types'

export const mutations = {
  [INIT] (state) {
    state.all = []
  },

  [FETCH_ALL] (state, records) {
    state.all = records
  },

  [FETCH] (state, record) {
    const index = state.all.findIndex(x => x.id === record.id)
    if (index === -1) {
      state.all.push(record)
    } else {
      state.all.splice(index, 1, record)
    }
  },

  [CREATE] (state, record) {
    const index = state.all.findIndex(x => x.id === record.id)
    if (index === -1) {
      state.all.push(record)
    }
  },

  [UPDATE] (state, record) {
    const index = state.all.findIndex(x => x.id === record.id)
    if (index !== -1) {
      state.all.splice(index, 1, record)
    }
  },

  [DELETE] (state, record) {
    state.all = state.all.filter(x => x.id !== record.id)
  }
}
