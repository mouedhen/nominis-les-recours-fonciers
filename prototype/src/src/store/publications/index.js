import Publication from '../../core/models/acts/publication'

import {
  INIT,
  FETCH_ALL,
  CREATE,
  UPDATE,
  DELETE
} from './mutations-types'
import { state } from './state'
import { mutations } from './mutations'
import { getters } from './getters'

const actions = {
  async fetchAll (context) {
    (new Publication({})).fetchAll()
      .then(records => {
        context.commit(FETCH_ALL, records)
      })
      .catch(error => {
        console.log(error)
      })
  },

  async syncCollection (context, params = {filters: null, limit: null, orderBy: null, order: 'asc'}) {
    context.commit(INIT)
    let query = new Publication({}).queryBuilder(params.filters)
    if (params.orderBy) {
      query.orderBy(params.orderBy, params.order)
    }
    if (params.limit !== null && Number.isInteger(params.limit)) {
      query = query.limit(params.limit)
    }
    return query.onSnapshot(doc => {
      doc.docChanges().forEach(snapshot => {
        switch (snapshot.type) {
          case 'added':
            return context.commit(CREATE, new Publication({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'removed':
            return context.commit(DELETE, new Publication({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'modified':
            if (snapshot.oldIndex !== snapshot.newIndex) {
              context.commit(DELETE, new Publication({id: snapshot.oldIndex, ...snapshot.doc.data()}))
              return context.commit(CREATE, new Publication({id: snapshot.newIndex, ...snapshot.doc.data()}))
            } else {
              return context.commit(UPDATE, new Publication({id: snapshot.doc.id, ...snapshot.doc.data()}))
            }
        }
      })
    })
  },

  async fetchQuery (context, filters) {
    (new Publication({})).fetchQuery(filters)
      .then(acts => {
        context.commit(FETCH_ALL, acts)
      })
      .catch(error => {
        console.log(error)
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
