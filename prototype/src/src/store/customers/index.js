import firestore from '../../firestore'
import Customer from '../../core/models/customers/Customer'

import { INIT, FETCH_ALL, CREATE, UPDATE, DELETE } from './mutations-types'
import { state } from './state'
import { mutations } from './mutations'
import { getters } from './getters'

const actions = {
  async fetchAll (context) {
    (new Customer({})).fetchAll().then(customers => {
      context.commit(FETCH_ALL, customers)
    }).catch(error => {
      console.log(error)
    })
  },

  async syncCollection (context) {
    context.commit(INIT)
    return firestore.collection('customers').onSnapshot(doc => {
      doc.docChanges().forEach(snapshot => {
        switch (snapshot.type) {
          case 'added':
            return context.commit(CREATE, new Customer({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'removed':
            return context.commit(DELETE, new Customer({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'modified':
            if (snapshot.oldIndex !== snapshot.newIndex) {
              context.commit(DELETE, new Customer({id: snapshot.oldIndex, ...snapshot.doc.data()}))
              return context.commit(CREATE, new Customer({id: snapshot.newIndex, ...snapshot.doc.data()}))
            } else {
              return context.commit(UPDATE, new Customer({id: snapshot.doc.id, ...snapshot.doc.data()}))
            }
        }
      })
    })
  },

  async fetchQuery (context, filters) {
    (new Customer({})).fetchQuery(filters).then(customers => {
      context.commit(FETCH_ALL, customers)
    }).catch(error => {
      console.log(error)
    })
  },

  async create (context) {
    (new Customer({})).create().then(customer => {
      context.commit(CREATE, customer)
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
