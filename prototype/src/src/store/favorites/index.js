import firestore from '../../firestore'
import { INIT, CREATE, UPDATE, DELETE } from './mutations-types'
import { state } from './state'
import { mutations } from './mutations'
import { getters } from './getters'
import Favorite from '../../core/models/customers/favorite'

const actions = {
  async syncCollection (context, uid) {
    context.commit(INIT)
    return firestore.collection('favorites').where('userId', '==', uid).onSnapshot(doc => {
      doc.docChanges().forEach(snapshot => {
        switch (snapshot.type) {
          case 'added':
            return context.commit(CREATE, new Favorite({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'removed':
            return context.commit(DELETE, new Favorite({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'modified':
            if (snapshot.oldIndex !== snapshot.newIndex) {
              context.commit(DELETE, new Favorite({id: snapshot.oldIndex, ...snapshot.doc.data()}))
              return context.commit(CREATE, new Favorite({id: snapshot.newIndex, ...snapshot.doc.data()}))
            } else {
              return context.commit(UPDATE, new Favorite({id: snapshot.doc.id, ...snapshot.doc.data()}))
            }
        }
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
