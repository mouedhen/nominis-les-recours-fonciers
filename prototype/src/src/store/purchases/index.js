import {
  INIT,
  CREATE,
  UPDATE,
  DELETE
} from './mutations-types'
import { state } from './state'
import { mutations } from './mutations'
import { getters } from './getters'
import firestore from '../../firestore'
import Purchase from '../../core/models/customers/purchase'

const actions = {
  async syncCollection (context, uid) {
    context.commit(INIT)
    return firestore.collection('purchases').where('userId', '==', uid).onSnapshot(doc => {
      doc.docChanges().forEach(snapshot => {
        switch (snapshot.type) {
          case 'added':
            return context.commit(CREATE, new Purchase({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'removed':
            return context.commit(DELETE, new Purchase({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'modified':
            if (snapshot.oldIndex !== snapshot.newIndex) {
              context.commit(DELETE, new Purchase({id: snapshot.oldIndex, ...snapshot.doc.data()}))
              return context.commit(CREATE, new Purchase({id: snapshot.newIndex, ...snapshot.doc.data()}))
            } else {
              return context.commit(UPDATE, new Purchase({id: snapshot.doc.id, ...snapshot.doc.data()}))
            }
        }
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
