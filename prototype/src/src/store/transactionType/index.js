import firestore from '../../firestore'

import { CREATE, DELETE, INIT, UPDATE } from './mutations-types'
import { state } from './state'
import { mutations } from './mutations'
import { getters } from './getters'
import TransactionType from '../../core/models/acts/const/transactionType'

const actions = {
  async syncCollection (context) {
    context.commit(INIT)
    return firestore.collection('config').doc('configNominisResidentielle').collection('transactionsTypes').onSnapshot(doc => {
      doc.docChanges().forEach(snapshot => {
        switch (snapshot.type) {
          case 'added':
            return context.commit(CREATE, new TransactionType({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'removed':
            return context.commit(DELETE, new TransactionType({id: snapshot.doc.id, ...snapshot.doc.data()}))
          case 'modified':
            if (snapshot.oldIndex !== snapshot.newIndex) {
              context.commit(DELETE, new TransactionType({id: snapshot.oldIndex, ...snapshot.doc.data()}))
              return context.commit(CREATE, new TransactionType({id: snapshot.newIndex, ...snapshot.doc.data()}))
            } else {
              return context.commit(UPDATE, new TransactionType({id: snapshot.doc.id, ...snapshot.doc.data()}))
            }
        }
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
