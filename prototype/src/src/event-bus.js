import PubSub from './core/utils/pub-sub'
import Publication from './core/models/acts/publication'
// import Document from './core/models/storage/document'

let eventBus = new PubSub()

eventBus.subscribe('publish-lot', function (lot) {
  return new Publication({...lot})
    .createWithID(lot.id)
    .catch(e => {
      console.log(e)
    })
})

eventBus.subscribe('unpublish-lot', function (lotID) {
  return new Publication({id: lotID})
    .delete()
    .catch(e => {
      console.log(e)
    })
})

eventBus.subscribe('delete-attachment', function (name) {
  // return Document.deleteFile(name)
  //   .catch(e => {
  //     console.log(e)
  //   })
})

export default eventBus
