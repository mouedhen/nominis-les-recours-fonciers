import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/messaging'

const env = process.env.NODE_ENV

console.log('env', env)

let config = {}

if (env === 'development') {
  config = {
    apiKey: 'AIzaSyCmdcN9r7TDJHsqlOdDItAnVvq9LSgFlZY',
    authDomain: 'nominis-dev.firebaseapp.com',
    databaseURL: 'https://nominis-dev.firebaseio.com',
    projectId: 'nominis-dev',
    storageBucket: 'nominis-dev.appspot.com',
    messagingSenderId: '161318739728'
  }
} else {
  config = {
    apiKey: 'AIzaSyD1wf7M3zQTK8c4a-4hxSKQyYs2laMQ_ww',
    authDomain: 'nominis-abee2.firebaseapp.com',
    databaseURL: 'https://nominis-abee2.firebaseio.com',
    projectId: 'nominis-abee2',
    storageBucket: 'nominis-abee2.appspot.com',
    messagingSenderId: '48687453989'
  }
}

config = {
  apiKey: 'AIzaSyD1wf7M3zQTK8c4a-4hxSKQyYs2laMQ_ww',
  authDomain: 'nominis-abee2.firebaseapp.com',
  databaseURL: 'https://nominis-abee2.firebaseio.com',
  projectId: 'nominis-abee2',
  storageBucket: 'nominis-abee2.appspot.com',
  messagingSenderId: '48687453989'
}

firebase.initializeApp(config)

export default firebase
