import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp(functions.config().firebase);

export const createUser = functions.https.onCall((data, context) => {
    return admin.auth().createUser({
        email: data.email,
        emailVerified: true,
        displayName: data.role,
        password: data.password,
        disabled: data.isActive
    }).then(function (userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        return ({uid: userRecord.uid, user: userRecord});
    }).catch(function (error) {
        console.log("Error creating new user:", error);
    });
});
export const createCustomer = functions.https.onCall((data, context) => {
    return admin.auth().createUser({
        email: data.email,
        password: data.password,
        disabled: data.isActive
    }).then(function (userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        return ({uid: userRecord.uid, user: userRecord});
    }).catch(function (error) {
        console.log("Error creating new user:", error);
    });
});
export const updateUser = functions.https.onCall((data, context) => {
    return admin.auth().updateUser(data.uid, {
        displayName: data.role,
        disabled: data.isActive
    }).then(function (userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        return ({uid: userRecord.uid, user: userRecord});
    }).catch(function (error) {
        console.log("Error creating new user:", error);
    });
});
export const updateCustomer = functions.https.onCall((data, context) => {
    return admin.auth().updateUser(data.uid, {
        disabled: data.isActive
    }).then(function (userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        return ({uid: userRecord.uid, user: userRecord});
    }).catch(function (error) {
        console.log("Error creating new user:", error);
    });
});
export const deleteUser = functions.https.onCall((data, context) => {
    return admin.auth().deleteUser(data.uid).then(function () {
        return true;
    }).catch(function (error) {
        console.log("Error creating new user:", error);
        return false;
    });
});
