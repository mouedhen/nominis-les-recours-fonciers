"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const nodemailer = require("nodemailer");
admin.initializeApp(functions.config().firebase);
// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript
exports.sendEmail = functions.https.onCall((data, context) => {
    const mailTransport = nodemailer.createTransport(`smtps://karim.dahmani6@gmail.com:copperFISh45?!@smtp.gmail.com`);
    const mailOptions = {
        from: `<noreply@newthinkingit.com>`,
        to: 'contact@newthinkingit.com'
    };
    // hmtl message constructions
    mailOptions.subject = 'contact form message';
    /*mailOptions.html = `<p><b>Name: </b>Hi hi hi</p>
                      <p><b>Email: </b>${body.rsEmail}</p>
                      <p><b>Subject: </b>${body.rsSubject}</p>
                      <p><b>Message: </b>${body.rsMessage}</p>`;*/
    return mailTransport.sendMail(mailOptions);
});
exports.createUser = functions.https.onCall((data, context) => {
    return admin.auth().createUser({
        email: data.email,
        emailVerified: true,
        displayName: data.role,
        password: data.password,
        disabled: data.isActive
    }).then(function (userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        return ({ uid: userRecord.uid, user: userRecord });
    }).catch(function (error) {
        console.log("Error creating new user:", error);
    });
});
exports.createCustomer = functions.https.onCall((data, context) => {
    return admin.auth().createUser({
        email: data.email,
        password: data.password,
        disabled: data.isActive
    }).then(function (userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        return ({ uid: userRecord.uid, user: userRecord });
    }).catch(function (error) {
        console.log("Error creating new user:", error);
    });
});
exports.updateUser = functions.https.onCall((data, context) => {
    return admin.auth().updateUser(data.uid, {
        displayName: data.role,
        disabled: data.isActive
    }).then(function (userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        return ({ uid: userRecord.uid, user: userRecord });
    }).catch(function (error) {
        console.log("Error creating new user:", error);
    });
});
exports.updateCustomer = functions.https.onCall((data, context) => {
    return admin.auth().updateUser(data.uid, {
        disabled: data.isActive
    }).then(function (userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
        return ({ uid: userRecord.uid, user: userRecord });
    }).catch(function (error) {
        console.log("Error creating new user:", error);
    });
});
exports.deleteUser = functions.https.onCall((data, context) => {
    return admin.auth().deleteUser(data.uid).then(function () {
        return true;
    }).catch(function (error) {
        console.log("Error creating new user:", error);
        return false;
    });
});
/*
export const sendNewPublicationNotifications = functions.firestore.document('acts/{actId}').onUpdate((change, context) => {
    const data = change.after.data();
    const lastData = change.before.data();
    const payload = {
        notification: {
            title: `Nouvelle publication`,
            body: data.transactionType + ', matricule : ' + data.registrationNumber
            // clickAction: ''
        }
    };
    if (data.publishedBy && !lastData.publishedBy) {
        return admin.firestore().collection('customers').get().then(snapshot => {
            let notificationTokens = [];
            snapshot.forEach(doc => {
                const notificationToken = doc.data().notificationToken;
                if (notificationToken) {
                    notificationTokens.push(notificationToken);
                }
            })
            notificationTokens.forEach(notificationToken => {
                console.log('send notification to', notificationToken);
                return admin.messaging().sendToDevice(notificationToken, payload);
            })
        })
    } else {
        console.log('null');
        return null;
    }
});

const sendEmail = functions.https.onCall((data, context) => {
    const data1 = {
        from: 'karim.dahmani@newthinkingit.com',
        subject: 'Nouvelles publications',
        html: `<p>Welcome!</p>`,
        // 'h:Reply-To': 'app@app.com',
        to: 'karim.dahmani6@gmail.com'
    };
    const mg = new Mailgun({apiKey: '9c53bd245c76c373cfec0e4d5a250759-770f03c4-e91a207a'});
    mg.messages().send(data1, function (error, body) {
        console.log(body);
    });
});
export const sendEmailAtGivenTimes = functions.https.onCall((data, context) => {
    const times = data.times;
    ontime({
        cycle: times
    }, function(ot) {
        sendEmail(null, null);
        ot.done();
        return
    });
});*/ 
//# sourceMappingURL=index.js.map