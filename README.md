# Nominis - Les recours fonciers +

> v1.0.0

## Structures des dossiers

- docs : Documents relatifs à l'application

- api.nominis : Serveur de l'application

- app.nominis : Partie client de l'application

- prototype : Prototype réalisé avec VueJs et Firestore

- screenshots : Captures d'écran

## Installation

- MongoDB : version 4.0.4
- NodeJs : version 11.2.0
- npm : version 6.4.1

## Développement
